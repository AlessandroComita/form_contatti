let eBottoneInoltra = document.getElementById('bottoneInoltra');

eBottoneInoltra.addEventListener("click", controllo);


function controllo()
{
    let e_cognome = document.modulo.cognome.value;
    let e_nome = document.modulo.nome.value;
    let e_matricola = document.modulo.matricola.value;
    let e_regione = document.modulo.regione.value;
    let e_email = document.modulo.email.value;
    let e_telefono = document.modulo.telefono.value;
    
    if (!e_cognome)
    {
        alert('il campo Cognome non può essere vuoto');
    }

    if (!e_nome)
    {
        alert('il campo Nome non può essere vuoto');
    }

    // questo controllo non era richiesto, ma glielo metto lo stesso perché mi sembra importante
    if (!e_matricola)
    {
        alert('il campo Matricola non può essere vuoto');
    }

    if  (isNaN(e_matricola))
    {
        alert('il campo Matricola deve essere un numero');
    }

    if (!e_email)
    {
        alert('il campo Email non può essere vuoto');
    }

    if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(e_email)))
    {
        alert("L'indirizzo email inserito non è valido")
    }

    if (!e_telefono)
    {
        alert('il campo Telefono non può essere vuoto');
    }

    if (!e_regione)
    {
        alert('il campo Regione non può essere vuoto');
    }
}